package TestNG;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Google_search {
	
	public static WebDriver driver = null;
	WebDriverWait wait = null;
	
	@BeforeTest
	public void driverInit() {
		
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\veperam.ORADEV\\Desktop\\geckodriver.exe");
		driver = new FirefoxDriver();
		wait= new WebDriverWait(driver,10);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get("https://www.google.com/?gws_rd=ssl");
	}
	
	@Test
	public void pageTitle() {
		
		driver.getTitle();
		
	}
	
	@Test
	public void searchText() {
		
		driver.findElement(By.xpath("//input[@name='q']")).sendKeys("JavaSelenium");
		WebElement resultOfArray = driver.findElement(By.xpath("//div/ul[@role='listbox']"));
		wait.until(ExpectedConditions.elementToBeClickable(resultOfArray));
		List<WebElement> arrayElements = driver.findElements(By.xpath("//div/ul[@role='listbox']/li"));
		
		for(WebElement ele: arrayElements) {
			System.out.println(ele.getText());
			if(ele.getText().equalsIgnoreCase("java selenium tutorial")) {
				ele.click();
				System.out.println(driver.getTitle());
				break;
			}
			
		}
		
	}

	@AfterTest
	public void closeBroser() {
		driver.close();
	}
	
}
