package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;

public class NewTest {
	
  public static WebDriver driver = null;
  public static String URL = "https://www.training-support.net/selenium/";
  
  @Test
  public void f() {
	  
	  driver.get(URL);
	  
  }
  
  @Test
  public void f2() {
	  
	  driver.getTitle();
	  System.out.println(driver.getTitle());
	  
  }
  
  @Test
  public void f3() {
	  
	  WebElement ele = driver.findElement(By.name("Search"));
	  
  }
  
  @Test(enabled = false)
  public void f4() {
	  
	  driver.get(URL);
	  
  }
  
  @Test
  public void f5() throws SkipException{
	  
	  driver.get(URL);
	  throw new SkipException("This method will be skipped");
	  
  }
  
  @BeforeTest
  public void beforeTest() {
	  System.setProperty("webdriver.gecko.driver", "C:\\Users\\veperam.ORADEV\\Desktop\\geckodriver.exe");
	  driver = new FirefoxDriver();
	  driver.manage().window().maximize();
  }

  @AfterTest
  public void afterTest() {
	  
	  driver.close();
  }

}
