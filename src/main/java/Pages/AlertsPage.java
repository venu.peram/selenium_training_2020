package Pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AlertsPage extends BasePage{
	
	// Constructor 
	public AlertsPage(WebDriver driver) {
		super(driver);
	}
	
	// BaseUrl
	
			public String URL = "https://www.training-support.net/selenium/javascript-alerts";
			
	// Page variables
	public By simpleAlertButton = By.xpath("//button[@id='simple']");
	public By confirmAlertButton = By.xpath("//button[@id='confirm']");
	public By promptAlertButton = By.xpath("//button[@id='prompt']");
	
	// Page Elements 
	public AlertsPage goToPage() {
		driver.get(this.URL);
		return this;
	}

	public WebElement getButton(By element) {
		WebElement button = driver.findElement(element);
		return button;
	}
	
	public void getAlertTextAccept() {
		Alert simpleAlert = driver.switchTo().alert();
		System.out.println(simpleAlert.getText());
		simpleAlert.accept();
		
	}
	
	
}
