package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage{
	
	// BaseUrl
	public String URL = "https://www.training-support.net/selenium/login-form";
	public String pageTitle = "Login Form";
	public String failureConfrirm = "Invalid Credentials";
	public String successConfirm = "Welcome Back, admin";
	public String result = "";
	
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	// Page Elements 
	public LoginPage goToPage() {
	 driver.get(this.URL);
	 return this;
	}
	
	// Locators of LoginPage
	public By userame = By.id("username");
	public By password = By.id("password");
	public By loginBtn = By.xpath("//button[contains(text(),'Log in')]");
	public By actionConfirmation = By.xpath("//div[@id='action-confirmation']");

	// Methods for logIn Page
	public String logIn(String userName, String passWord) {
		driver.findElement(userame).sendKeys(userName);
		driver.findElement(password).sendKeys(passWord);
		click(loginBtn);
		result = driver.findElement(actionConfirmation).getText();
		return result;
	}
	
}
