package com.selenium.UTILS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseTest {
	
	public static WebDriver driver = null;
	
	
	public static void openUrl(String URL, String Driver) {
		System.setProperty("driver.gecko.driver", "C:\\Users\\veperam.ORADEV\\Desktop\\geckodriver.exe");
		if(Driver.equalsIgnoreCase("FireFox")) {
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.get(URL);
		}
		
	}
	
	public static WebDriver getDriver()
	{
	    return driver;
	}
	
	public static void closeBrowser() {
		driver.close();
	}

}
