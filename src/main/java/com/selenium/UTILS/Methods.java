package com.selenium.UTILS;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.text.html.HTMLDocument.Iterator;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.selenium.UTILS.*;

public class Methods {
	
	public static void LoginForm(WebDriver driver) {
		
		driver.findElement(By.cssSelector("div.ui.vertical.segment.spacer > div >div:nth-child(6)")).click();
		System.out.println("THE TITLE OF LOGIN FORM PAGE IS:::::"+driver.getTitle());
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("password");
		driver.findElement(By.xpath("//button[contains(text(), 'Log in')]")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//div[contains(text(), 'Welcome Back, admin')]")).isEnabled();
		
	}
	
    public static void JavaScriptAlerts(WebDriver driver) {
		
    	driver.findElement(By.cssSelector("div.ui.vertical.segment.spacer > div >div:nth-child(1)")).click();
    	System.out.println(driver.getTitle());
    	
    	/* Below code works for SimpleAlert */
    	driver.findElement(By.xpath("//button[@id='simple']")).click();
    	Alert simpleAlert = driver.switchTo().alert();
    	System.out.println(simpleAlert.getText());
    	simpleAlert.accept();
    	
    	/* Below code works for Confirmation alert */
    	driver.findElement(By.xpath("//button[@id='confirm']")).click();
    	Alert confirmAlert = driver.switchTo().alert();
    	System.out.println(confirmAlert.getText());
    	confirmAlert.dismiss();
    	
    	/* Below code works for Promt alert */
    	driver.findElement(By.xpath("//button[@id='prompt']")).click();
    	Alert promptAlert = driver.switchTo().alert();
    	promptAlert.sendKeys("TEST");
    	System.out.println(promptAlert.getText());
    	promptAlert.accept();
		
	}
    
    public static void TabOpener(WebDriver driver) throws InterruptedException {
    	
    	driver.findElement(By.cssSelector("div.ui.vertical.segment.spacer > div > div:nth-child(2)")).click();
    	System.out.println(driver.getTitle());
    	
    	// Below code for newTab opens
    	
    	//System.out.println("Window handle is::::"+newtabTitle);
    	String newtabTitle = driver.getWindowHandle();
    	driver.findElement(By.xpath("//a[contains(text(), 'Click Me!')]")).click();
    	Thread.sleep(5000);
    	String handle1 = driver.getWindowHandle();
    	driver.switchTo().window(handle1);
    	Thread.sleep(5000);
    	//driver.findElement(By.cssSelector("#actionButton")).click();
    	Thread.sleep(5000);
    	
    	//driver.switchTo().window(newtabTitle);
    	String newtabTitle2 = driver.getWindowHandle();
    	System.out.println("Window handle is::::"+newtabTitle);
    	
    	System.out.println("Window Title is:::::"+driver.getTitle());
    	
    	
    	Set<String> windlowHandles = driver.getWindowHandles();
    	System.out.println(windlowHandles);
    	
    	for(String handle : windlowHandles) {
    		driver.switchTo().window(handle);
    		
    		if(!handle.equalsIgnoreCase(newtabTitle)) {
    			driver.close();
    		}
    		
    	}
    	//System.out.println(driver.getWindowHandle());
    	//System.out.println(driver.getTitle());
    	
    	//driver.quit();
    	
    }

}
