import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.selenium.UTILS.*;

public class Test {

	public static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		String URL = "https://www.training-support.net/selenium";
		driver = BaseTest.getDriver();
		
		BaseTest.openUrl(URL, "FireFox");
		BaseTest.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(BaseTest.getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(BaseTest.getDriver().findElement(By.xpath("//button[text()='Get Started!']"))));
		
		BaseTest.getDriver().findElement(By.xpath("//button[text()='Get Started!']")).click();
		
		//Methods.LoginForm(BaseTest.getDriver());
		//Methods.JavaScriptAlerts(BaseTest.getDriver());
		Methods.TabOpener(BaseTest.getDriver());
		
		//BaseTest.closeBrowser();
		

	}

}
