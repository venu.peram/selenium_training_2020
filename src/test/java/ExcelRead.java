import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ExcelRead {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		ExcelRead spreadsheet = new ExcelRead();
        String filePath = "D:\\Practice.xlsx";
                           
        

        System.setProperty("driver.gecko.driver", "C:\\Users\\veperam.ORADEV\\Desktop\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		String URL = "https://www.training-support.net/selenium/todo-list";
		
		driver.get(URL);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		spreadsheet.readExcel(filePath, "Sheet1",driver);
		
		driver.close();
	}
	
	public void readExcel(String filePath, String sheetName, WebDriver driver) throws IOException {
        // Create an object of File class to open the XLSX file
        File file = new File(filePath);
 
        // Create an object of FileInputStream class to read excel file
        FileInputStream inputStream = new FileInputStream(file);
 
        // Create a Workbook
        XSSFWorkbook book = null;
 
        // Find the file extension by splitting file name in substring and getting only extension name
        String fileExtensionName = filePath.substring(filePath.indexOf("."));
 
        // Check if it is an xlsx file
        if(fileExtensionName.equals(".xlsx")) {
 
            // If it's an xlsx file, create object of XSSFWorkbook class
            book = new XSSFWorkbook(inputStream);
 
        }
 
        // Read the sheet inside by it's name
        XSSFSheet sheet = book.getSheet(sheetName);
 
        // Find the number of rows in the file
        int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
 
        // Create a loop over all the rows to read it
        for (int i = 0; i < rowCount+1; i++) {
            XSSFRow row = sheet.getRow(i);
            
            //Create a loop to print cell values in a row
            for (int j = 0; j < row.getLastCellNum(); j++) {
                //Print Excel Data into the Console
                System.out.print(row.getCell(j).getStringCellValue() + " || ");
                
                
        		driver.findElement(By.xpath("//input[@id='taskInput']")).sendKeys(row.getCell(j).getStringCellValue());
        		driver.findElement(By.xpath("//button[@class='ui blue button']")).click();
         		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				  driver.findElement(By.xpath("//div[text()='A']")).isDisplayed();
				  driver.findElement(By.xpath("//div[text()='B']")).isDisplayed();
				  driver.findElement(By.xpath("//div[text()='C']")).isDisplayed();
				  driver.findElement(By.xpath("//div[text()='D']")).isDisplayed();
				  driver.findElement(By.xpath("//div[text()='E']")).isDisplayed();
				 
        		
            }
           
            System.out.println();
        }
 
    }

}
