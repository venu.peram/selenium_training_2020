package automationpractice;

import java.awt.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.List.*;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class SFPage {

	public static String URL = "http://automationpractice.com/index.php";
	public static WebDriver driver = null;
	public static WebDriverWait wait = null;
	
	/*
	 * Locaters and constants
	 */
	
	public static String TITLEPAGE = "My Store";
	public static String PROD_SEARCH_BOX = "#search_query_top";  //CSSSelector
	public static String CART = "//a/b[text()='Cart']"; //XPath selector
	public static String ADD_TO_CART = "//button/span[text()='Add to cart']"; // Xpath selector
	
	
	/*
	 * This method is for initialize the DRIVER and launch the URL of the given site and Assert title of the PAGE
	 */
	@BeforeMethod
	public void initializeDriver() {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\veperam.ORADEV\\Desktop\\geckodriver.exe");
		driver = new FirefoxDriver();
		wait= new WebDriverWait(driver,10);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(URL);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(CART))));
		Assert.assertEquals(driver.getTitle(), TITLEPAGE);
		Assert.assertTrue(driver.findElement(By.xpath(CART)).isDisplayed());
	}
	
	/*
	 * This method is for close the DRIVER instance after all TEST methods executed
	 */
	@AfterMethod
	public void closeBroser() {
		driver.close();
	}
	
	/*
	 * This method is for search for a product / item in parent SF page
	 */
	@Test
	public void search_product_TestCase1() throws InterruptedException {
		
		
		driver.findElement(By.cssSelector(PROD_SEARCH_BOX)).sendKeys("Printed Chiffon Dress");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ac_even")));
		driver.findElement(By.cssSelector(".ac_odd")).click();
		
	}
		
	@Test
	public void add_item_to_cart_TestCase2() throws InterruptedException {
	
		search_product_TestCase1();
		
		WebElement sizeDropdown = driver.findElement(By.xpath("//div[@id='uniform-group_1']/select"));
		Select selectDropDown = new Select(sizeDropdown);
		selectDropDown.selectByValue("3");
        Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@id='color_8']")).click();
		driver.findElement(By.xpath(ADD_TO_CART)).click();
		
	}
		
	@Test
	public void remove_item_from_cart_checkout_TestCase3() throws InterruptedException {
	
		add_item_to_cart_TestCase2();
		
		Thread.sleep(2000);
		WebElement we = driver.findElement(By.xpath("//span[contains(text(), 'Proceed to checkout')]"));
		wait.until(ExpectedConditions.elementToBeClickable(we));
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", we);
		
		Thread.sleep(5000);
		WebElement cartEle = driver.findElement(By.xpath(CART));
		Actions act = new Actions(driver);
		
		act.moveToElement(cartEle).build().perform();
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div/div[1]/header/div[3]/div/div/div[3]/div/div/div/div/dl/dt/span/a")).click();
		
	}
	
	@Test
	public void heckout_TestCase4() throws InterruptedException {
	
		add_item_to_cart_TestCase2();
		
		WebElement we = driver.findElement(By.xpath("//span[contains(text(), 'Proceed to checkout')]"));
		wait.until(ExpectedConditions.elementToBeClickable(we));
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", we);
		
		Thread.sleep(5000);
		WebElement cartEle = driver.findElement(By.xpath(CART));
		
		driver.findElement(By.xpath("//span[text() = 'Proceed to checkout']")).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@id='email_create']"))));
		driver.findElement(By.xpath("//input[@id='email_create']")).sendKeys("test@test.com");
		driver.findElement(By.xpath("//button[@id='SubmitCreate']")).click();
		
	}
		
		
		
		//System.out.println("TTTTTTTTTTTT" +driver.getTitle());
		
		
		//driver.findElement(By.cssSelector("#button_order_cart > span:nth-child(1)")).click();
		
		
		
		/*
		 * JavascriptExecutor js = (JavascriptExecutor) driver;
		 * js.executeScript("arguments[0].click();", we);
		 */
		//driver.findElement(By.cssSelector("span.cross")).click();
		
		////////////////////a.btn:nth-child(2) > span:nth-child(1)
		/*
		 * System.out.println("SECOND WINDOW::::::::::::::::"+driver.getWindowHandles())
		 * ; Alert alert = driver.switchTo().alert();
		 */	//div[@class='clearfix']/div[2]/div[4]/a/span
		//driver.findElement(By.xpath("//span[contains(text(), 'Proceed to checkout')]")).click();
		//System.out.println(driver.findElement(By.xpath("//div[@class='clearfix']/div/h2/i")).getText());
		
		//driver.findElement(By.cssSelector(".button-medium > span")).click();
		
		
		
		
		
	
	
}
