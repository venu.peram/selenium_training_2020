package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import Pages.LoginPage;

public class LoginTest extends BaseTest {

	@Test
	public void successLogIn() {
		LoginPage page = new LoginPage(driver);
		String result = page.goToPage().logIn("admin", "password");
		Assert.assertEquals(result, page.successConfirm);
	}
	
	@Test(retryAnalyzer  = retryAnalyzer.class)
	public void failureLogIn() {
		LoginPage page = new LoginPage(driver);
		String result = page.goToPage().logIn("admin", "admin");
		Assert.assertEquals(result, page.failureConfrirm);
	}
}
