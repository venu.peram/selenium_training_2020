package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
//
import java.util.Properties;

import Pages.AlertsPage;

public class BaseTest {

	public WebDriver driver=null;
	public WebDriverWait wait;
	
	File file = new File("testProp.properties");
	FileInputStream fileInput = null;
	Properties prop = new Properties();
	
	@BeforeTest
	public void setUp() throws IOException{
		  		  
		prop = readFile();	
			
		if( (prop.getProperty("Browser")).equalsIgnoreCase("chromeBrowser")){
			
			driver = new ChromeDriver();
			
		}
		
        if( (prop.getProperty("Browser")).equalsIgnoreCase("Firefox")){
			
			driver = new FirefoxDriver();
			
		}

		  //System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		  //driver = new FirefoxDriver();
		  
		  driver.manage().window().maximize();
		  driver.manage().deleteAllCookies();
		  
	}
	
	@AfterTest
	public void killBrowser() {
		
		driver.close();
		
	}
	
	/* This method is used for FILE READING */
	public Properties readFile() {
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			e.getMessage();
		}
		//load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
}
