package Tests;

import org.testng.annotations.Test;

import Pages.AlertsPage;

public class AlertsTest extends BaseTest{
	
	
	@Test
	public void simpleAlertTest() {
		AlertsPage page = new AlertsPage(driver);
		page.goToPage().getButton(page.simpleAlertButton).click();
		page.getAlertTextAccept();
	}

	@Test
	public void confirmAlertTest() {
		AlertsPage page = new AlertsPage(driver);
		page.goToPage().getButton(page.confirmAlertButton).click();
		page.getAlertTextAccept();
	}
	
	@Test
	public void promptAlertTest() {
		AlertsPage page = new AlertsPage(driver);
		page.goToPage().getButton(page.promptAlertButton).click();
		page.getAlertTextAccept();
	}

}
