package Tests;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Dup {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String[] str = {"A","B","C","A","B","D","D","D","A"};
		FindDuplicates(str, str.length);

	}

	public static void FindDuplicates(String[] str, int len) {
		// TODO Auto-generated method stub
		Map<String, Integer> map = new HashMap<String,Integer>();
		
		for(int i=0;i<len;i++) {
			
			if(map.containsKey(str[i]) )
			{
				map.put(str[i], map.get(str[i])+1);
				
			}
			else
			{
				map.put(str[i],1);
			}
		}
		
		for(Entry<String,Integer> entry:map.entrySet()) {
			
			if(entry.getValue()>1) {
				System.out.println(entry.getKey()+" Repeated "+ entry.getValue() +" Times");
				
			}
		}
	}

}
