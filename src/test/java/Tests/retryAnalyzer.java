package Tests;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class retryAnalyzer implements IRetryAnalyzer{
	
	int counter = 0;
	int maxRetry = 2;
	
	@Override
	public boolean retry(ITestResult rsult) {
		
		if(counter < maxRetry) {
			counter ++;
			return true;
			
		}
		
		return false;
	}
	
}
